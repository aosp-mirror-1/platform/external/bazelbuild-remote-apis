// Copyright 2024 The Android Open Source Project
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//      http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package {
    default_applicable_licenses: ["Android-Apache-2.0"],
}

java_library_host {
    name: "remote-execution-java-proto",
    srcs: [
        "remote_execution.proto",
    ],
    static_libs: [
        "googleapis-annotations-java-proto",
        "googleapis-operations-java-proto",
        "googleapis-status-java-proto",
        "semver-java-proto",
    ],
    libs: [
        "libprotobuf-java-full",
    ],
    proto: {
        include_dirs: [
            "external/bazelbuild-remote-apis/",
            "external/googleapis",
            "external/protobuf/src",
        ],
        type: "full",
    },
    // TODO(b/339514031): Unpin tradefed dependencies to Java 11.
    java_version: "11",
}

java_library_host {
    name: "remote-execution-java-grpc",
    srcs: [
        "remote_execution.proto",
    ],
    proto: {
        include_dirs: [
            "external/bazelbuild-remote-apis/",
            "external/googleapis",
            "external/protobuf/src",
        ],
        plugin: "grpc-java-plugin",
    },
    libs: [
        "grpc-java",
        "guava",
        "javax-annotation-api-prebuilt-host-jar",
        "libprotobuf-java-full",
        "remote-execution-java-proto",
    ],
    // TODO(b/339514031): Unpin tradefed dependencies to Java 11.
    java_version: "11",
}
